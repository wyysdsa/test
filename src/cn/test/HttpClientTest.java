package cn.test;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;



@SuppressWarnings("deprecation")
public class HttpClientTest {

	// 需要的图片路径
	private String path;
	
	public String[] returnInfo(String path) throws Exception {
		
		this.path = path;
		
		String url = "http://61.139.105.138/CheckCode.aspx";

		String imgName = path+"img/verify.jpg";

		String arr[] = new String[3];
		
		// 获取验证码 cookies
		arr = getVerify(url, imgName); 
		
		arr[2] = getInfo(arr[2]);
		
		return arr;
	}
	
	// 获取验证码
	public String[] getVerify(String url, String imgName) throws Exception{
		@SuppressWarnings("resource")
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet getMethod = new HttpGet(url);
		HttpResponse response = null;
		String arr[] = new String[3];
		response = httpClient.execute(getMethod);
		if("HTTP/1.1 200 OK".equals(response.getStatusLine().toString())){
			HttpEntity entity = response.getEntity();
			Header resCookie = response.getFirstHeader("Set-Cookie");
			if(resCookie!=null){
				arr[0] = "ok";
				arr[1] = resCookie.getValue().substring(0, 42);
				InputStream inputStream = entity.getContent();
				File file = new File(imgName);
				OutputStream outStream = new FileOutputStream(file);
				@SuppressWarnings("unused")
				int len = -1;
				byte bt[] = new byte[1024];
				while((len = inputStream.read(bt))!= -1){
					outStream.write(bt);
				}
				outStream.close();
				inputStream.close();
				arr[2] = imgName;
			}else{
				arr[0] = "failed";
			}
		}else{
			arr[0] = "failed";
		}
		return arr;
	}
	
	public String getInfo(String fileName) throws Exception
	{
		// 创建文件（图片）对象
		BufferedImage img;
		File file = new File(fileName);
		int width;
		int height;
		if(file.exists()){
			// 读出图片对象并且变为BufferedImage对象
			img = ImageIO.read(new File(fileName));
			img = img.getSubimage(5, 1, 50, img.getHeight()-2); 
			
			width = img.getWidth();
			height = img.getHeight();
			// 获取最小X Y 轴的值
			int minX = img.getMinX();
			int minY = img.getMinY();
//			System.out.println("图片的高度是：" + height + "图片的宽度是：" + width);		
			
//			去掉背景(包括干扰点)
			int arr[][] = new int[height][width];
			for(int i=minY; i<height; i++){
				for(int j=minX; j<width; j++){
					if(checkBlue(img.getRGB(j, i)) == 1){
						arr[i][j] = 1;
//						img.setRGB(j, i, Color.BLACK.getRGB());
					}else{
						arr[i][j] = 0;
//						img.setRGB(j, i, Color.WHITE.getRGB());
					}
				}
			}
			int flag;
			for(int i=0; i<height; i++){
				for(int j=0; j<width; j++){
					img.setRGB(j, i, Color.WHITE.getRGB());
					flag = 0;
					if(i!=0 && j!=0 && i<height-1 && j<width-1){// 去掉内部的干扰点
						if(arr[i][j] == 1){
							// 为了较为准确的去除干扰点，我们选择flag进行排除，
							// 若是超过三个方向都为0 则pass掉，进行消除
							if(arr[i-1][j] == 0){
								flag += 1;
							}
							if(arr[i][j+1] == 0){
								flag += 1;
							}
							if(arr[i+1][j] == 0){
								flag += 1;
							}
							if(arr[i][j-1] == 0){
								flag += 1;
							}
							if(flag > 3){
								arr[i][j] = 0;
								img.setRGB(j, i, Color.WHITE.getRGB());
							}else{
								img.setRGB(j, i, Color.BLACK.getRGB());
							}
						}else{
							img.setRGB(j, i, Color.WHITE.getRGB());
						}
					}else if(j == 0){ // 去掉左边的干扰点
						if(arr[i][j] == 1){
							arr[i][j] = 0;
							img.setRGB(j, i, Color.WHITE.getRGB());
						}
					}else if(i == height-1){ //去掉底部的干扰点
						if(arr[i][j] == 1){
							arr[i][j] = 0;
							img.setRGB(j, i, Color.WHITE.getRGB());
						}
					}else if(j == width-1){ //去掉右边的干扰点
						if(arr[i][j] == 1){
							arr[i][j] = 0;
							img.setRGB(j, i, Color.WHITE.getRGB());
						}
					}else{
						if(arr[i][j] == 1){
							arr[i][j] = 0;
							img.setRGB(j, i, Color.WHITE.getRGB());
						}
					}
				}
			}
			
			
			// 进行保存图片方便查看
			BufferedImage newImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_BGR);
			newImg.getGraphics().drawImage(img, 0, 0, width, height, null);
			File newFile = new File(path + "img/test.jpg");
			if(newFile.exists()){
				ImageIO.write(newImg, "jpg", newFile);
			}else{
				newFile.createNewFile();
				ImageIO.write(newImg, "jpg", newFile);
			}
			
			
			// 输出查看一下大致的模样
//			for(int i=0; i<height; i++){
//				for(int j=0; j<width; j++){
//					if(arr[i][j] == 0){
//						System.out.print("  ");
//					}else{
//						System.out.print("o ");
//					}
//				}
//				System.out.println();
//			}
			
			
			// 加载train 的img图片
			Map<BufferedImage, String> map= new HashMap<BufferedImage, String>();
			File file1 = new File(path + "img/trainimg");
			File[] fileArr = file1.listFiles();
			for(File img1 : fileArr){
				map.put(ImageIO.read(img1), img1.getName().charAt(0)+"");
			}
			
			// 因为每次只需要取四分之一，所以我们直接把宽度除以4
			String []arrFile = {
					"1.jpg", "2.jpg", "3.jpg", "4.jpg"
			};
			width = width/4;
			BufferedImage tmpImg;
//			 图片分割并且存在listImg数组里，后面进行单个单个的判断
			List<BufferedImage> listImg = new ArrayList<BufferedImage>();
			for(int i=0; i<4; i++){
				BufferedImage newImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_BGR);
				tmpImg = img.getSubimage(width*i, 0, width, height);
				newImage.getGraphics().drawImage(tmpImg, 0, 0, width, height, null);
				listImg.add(newImage);
				
				// 把分割了的图片进行保存查看是啥子样子
				File tmpImage = new File(path + "img/" + arrFile[i]);
				ImageIO.write(newImage, "jpg", tmpImage);
				
			}
			// 获取单个字符串
//			Set<BufferedImage> arr1 = map.keySet();
//			int flag2;
//			int flag1;
//			int rgb1;
//			int rgb2;
			String tmpResult;
			String result = "";
			for(BufferedImage imgCheck : listImg){
				tmpResult = "#";
				tmpResult = getSingleCharOcr(imgCheck, map);
//				originWidth = imgCheck.getWidth();
//				originHeight = imgCheck.getHeight();
//				String tmpResult = "#";
//				for(BufferedImage tmp : arr1){
//					flag2 = 0;
//					flag1 = 0;
//					for(int i=0; i<originHeight; i++){
//						for(int j=0; j<originWidth; j++){
//							rgb1 = imgCheck.getRGB(j, i);
//							rgb2 = tmp.getRGB(j, i);
//							if(isBlack(rgb1) != isBlack(rgb2)){
//								flag2 ++;
//							}
//						}
//					}
////					System.out.println(flag1 + " " +flag2);
////					return;
//					if(flag2 >= flag1-20 && flag2 <= flag1+20){
//						tmpResult = map.get(tmp);
//						result = result + tmpResult;
////						System.out.println(flag1 + " " +flag2);
////						return;
//						break;
//					}
//				}
				result += tmpResult;
			}
			return result;
		}else{
			return "文件不存在";
		}
	}
	
	// 判断黑色
	public int isBlack(int colorInt) {
		Color color = new Color(colorInt);
		if (color.getBlue() <= 5) {
			return 1;
		}
		return 0;
	}
	
	// 检查蓝色  是则返回1 否则返回0
//	public static int checkBlue(int rgb) throws Exception
//	{
//		Color color = new Color(rgb);
//		int rgbFinally = color.getBlue();
//		if(rgbFinally == 153){
//			return 1;
//		}else{
//			return 0;
//		}
//	}
	
	public int checkBlue(int colorInt) {  
        Color color = new Color(colorInt);  
        int rgb = color.getRed() + color.getGreen() + color.getBlue();
        if (rgb >= 140 && rgb <= 190) {  
            return 1;  
        }  
        return 0;  
    }
	
	
	
	public String getSingleCharOcr(BufferedImage img,
			Map<BufferedImage, String> map) {
			String result = "#";
			// 获取图片 尺寸， （高度，宽度）
			int width = img.getWidth();
			int height = img.getHeight();
			// 获取图片大小，（面积）
			int min = width * height;
			for (BufferedImage bi : map.keySet()) {
				int count = 0;
				if (Math.abs(bi.getWidth()-width) > 2)
					continue;
				int widthmin = width < bi.getWidth() ? width : bi.getWidth();
				int heightmin = height < bi.getHeight() ? height : bi.getHeight();
				Label1: for (int x = 0; x < widthmin; ++x) {
					for (int y = 0; y < heightmin; ++y) {
						if (isBlack(img.getRGB(x, y)) != isBlack(bi.getRGB(x, y))) {
							count++;
							if (count >= min)
								break Label1;
						}
					}
				}
				if (count < min) {
					min = count;
					result = map.get(bi);
				}
			}
			return result;
		}
	
	
	
	
	public void test() throws Exception
	{
//		Map<String, Integer> map = new HashMap<String, Integer>();
//		map.put("15", 0);
//		map.put("20", 1);
//		Map<String, Integer> map1 = new HashMap<String, Integer>();
//		map1.putAll(map);
//		for(String i : map1.keySet()){
//			System.out.println(i);
//		}
//		System.out.println(map1.keySet());
//		Map<BufferedImage, String> map= new HashMap<BufferedImage, String>();
//		File file = new File("trainimg");
//		File[] fileArr = file.listFiles();
//		for(File img : fileArr){
//			map.put(ImageIO.read(img), img.getName().charAt(0)+"");
//		}
//		Set<BufferedImage> arr = map.keySet();
//		int flag = 1;
//		for(BufferedImage tmp : arr){
//			System.out.print(map.get(tmp)+" ");
//			if(flag%5 == 0){
//				System.out.println();
//			}
//			flag ++;
//		}
		
		File file = new File("img/test.jpg");
		BufferedImage img = ImageIO.read(file);
		Color color = new Color(img.getRGB(10, 9));
		System.out.println(color.getRed());
	}
	
	
	
	
	
	
}






