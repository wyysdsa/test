package cn.test;

public class JsonEncode {
	// 数组转json字符串的方法
		public static String changeJson(String key[],Object []value) {
			String json = "{";
			int len = key.length;
			for(int i=0; i<len; i++) {
				json = json + changeJson(key[i], value[i]);
				if(i<len-1) {
					json = json + ","; 
				}
			}
			json = json+"}";
			return json;
		}
		
		public static String changeJson(String key[][],Object [][]value) {
			String json = "{";
			int len = key.length;
			for(int i=0; i<len; i++) {
				json = json + changeJson(key[i], value[i]);
				if(i<len-1) {
					json = json + ","; 
				}
			}
			json = json+"}";
			return json;
		}
		
		public static String changeJson(String key[][][],Object [][][]value) {
			String json = "{";
			int len = key.length;
			for(int i=0; i<len; i++) {
				json = json + changeJson(key[i], value[i]);
				if(i<len-1) {
					json = json + ","; 
				}
			}
			json = json+"}";
			return json;
		}
		
		public static String changeJson(String key, Object value) {
			return "\"" + key + "\"" + ":\""+ value + "\"";
		}
}
