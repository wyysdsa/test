package cn.test;

import javax.servlet.http.HttpServletRequest;

public class Test {
	private int i;
	public Test(int i) {
		this.i = i;
	}
	
//	public static void main(String args[]) {
//		System.out.println(new Test(5).getJson());
//	}
	
	public int getI() {
		return i;
	}
	
	public String getJson(Object value[]) {
		 String key[] = {"text", "cookie", "validate"};
		 
//		 Object value[] = {"ok", 0, 0};
		 
		 String str = JsonEncode.changeJson(key, value);
		 
		 return str;
	}
	
	// 获取ip的方法
	public String getIP(HttpServletRequest request) {
		String ip = "";
		if(request.getHeader("Proxy-Client-IP")!=null){
			ip = request.getHeader("Proxy-Client-IP");
			return ip;
		}
		
		if(request.getHeader("X-FORWARDED-FOR")!=null) {
			ip = request.getHeader("X-FORWARDED-FOR");
			return ip;
		}
		
		if(request.getHeader("WL-Proxy-Client-IP")!=null) {
			ip = request.getHeader("WL-Proxy-Client-IP");
			return ip;
		}
		
		if(request.getHeader("HTTP_CLIENT_IP")!=null) {
			ip = request.getHeader("HTTP_CLIENT_IP");
			return ip;
		}
		
		ip = request.getRemoteAddr();
		
		return ip;
	}
	
}
